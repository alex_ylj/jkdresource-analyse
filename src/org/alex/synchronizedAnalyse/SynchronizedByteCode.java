package org.alex.synchronizedAnalyse;

/**
 * synchronized源码分析
 */
public class SynchronizedByteCode {
    private static Object obj=new Object();

    /*
      对应字节码：
        public static void main(java.lang.String[]);
            Code:
               0: getstatic     #2                  // Field obj:Ljava/lang/Object;
               3: dup
               4: astore_1
               5: monitorenter
               6: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
               9: ldc           #4                  // String i am thread safe
              11: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
              14: aload_1
              15: monitorexit
              16: goto          24
              19: astore_2
              20: aload_1
              21: monitorexit
              22: aload_2
              23: athrow
              24: return
     */
    public static void main(String[] args) {
        synchronized (obj){
            System.out.println("i am thread safe");
        }
    }
}
